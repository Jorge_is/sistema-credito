import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { Solicitud } from '../models/solicitud';


@Injectable()
export class SolicitudService{
	public url: string;

	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}
	
	getSolicitudes(token, page){
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': token
		});

		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'solicitudes/'+page, options)
					.map(res => res.json());
	}

	getSolicitud(token, id:string){
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': token
		});

		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'solicitud/'+id, options)
					.map(res => res.json());
	}

	addSolicitud(token, solicitud: Solicitud){
		let params = JSON.stringify(solicitud);
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': token
		});

		return this._http.post(this.url+'solicitud', params, {headers : headers})
						.map(res => res.json());
	}

	editSolicitud(token, id:string , solicitud: Solicitud){
		let params = JSON.stringify(solicitud);
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': token
		});

		return this._http.put(this.url+'solicitud/'+id, params, {headers : headers})
						.map(res => res.json());
	}

	deleteSolicitud(token, id:string){
		let headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': token
		});

		let options = new RequestOptions({headers: headers});
		return this._http.delete(this.url+'solicitud/'+id, options)
					.map(res => res.json());
	}
}
