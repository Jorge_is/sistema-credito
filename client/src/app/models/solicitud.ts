export class Solicitud{
	constructor(
		public numero: string,
		public fecha: string,
		public personaFisica: string,
		public monto: string,
		public status: string,
		public score: string
	){}
}