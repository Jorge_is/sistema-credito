import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing, appRoutingProviders } from './app.routing';

import { HomeComponent } from './components/home.component';
import { AppComponent } from './app.component';
import { UserEditComponent } from './components/user-edit.component';
import { SolicitudListComponent } from './components/solicitud-list.component';
import { SolicitudAddComponent } from './components/solicitud-add.component';
import { SolicitudEditComponent } from './components/solicitud-edit.component';
import { SolicitudDetailComponent } from './components/solicitud-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    UserEditComponent,
    SolicitudListComponent,
    HomeComponent,
    SolicitudAddComponent,
    SolicitudEditComponent,
    SolicitudDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
