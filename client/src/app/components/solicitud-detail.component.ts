import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { SolicitudService } from '../services/solicitud.service';
import { Solicitud } from '../models/solicitud';

@Component({
	selector: 'solicitud-detail',
	templateUrl: '../views/solicitud-detail.html',
	providers: [UserService, SolicitudService]
})

export class SolicitudDetailComponent implements OnInit {
	public solicitud: Solicitud;
	public identity;
	public token;
	public url: string;
	public alertMessage;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _solicitudService: SolicitudService
	) {
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
	}

	ngOnInit(){
		console.log('solicitud-detail.component.ts cargado');
		//llamar al metodo del api para sacar una solicitud en base a su id getSolicitud
		this.getSolicitud();
	}

	getSolicitud(){
		this._route.params.forEach((params: Params) =>{
			let id = params['id'];

			this._solicitudService.getSolicitud(this.token, id).subscribe(
					response => {
						this.solicitud = response.solicitud;

						if (!response.solicitud) {
							this._router.navigate(['/']);
						}else{
							this.solicitud = response.solicitud;
						}
					},
					error =>{
						var errorMessage = <any>error;

			  			if (errorMessage != null) { 
			  				var body = JSON.parse(error._body);
			  				//this.alertMessage = body.message;
			  				
			  				console.log(error)
			  			}
					}
				);
		});
	}

}