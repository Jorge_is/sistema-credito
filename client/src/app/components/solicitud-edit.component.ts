import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { SolicitudService } from '../services/solicitud.service';
import { Solicitud } from '../models/solicitud';

@Component({
	selector: 'solicitud-edit',
	templateUrl: '../views/solicitud-add.html',
	providers: [UserService, SolicitudService]
})

export class SolicitudEditComponent implements OnInit {
	public titulo: string;
	public solicitud: Solicitud;
	public identity;
	public token;
	public url: string;
	public alertMessage;
	public is_edit;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _solicitudService: SolicitudService
	) {
		this.titulo = 'Editar solicitud';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		this.solicitud = new Solicitud('','','', '', '', '');
		this.is_edit = true;
	}

	ngOnInit(){
		console.log('solicitud-edit.component.ts cargado');
		//llamar al metodo del api para sacar una solicitud en base a su id getSolicitud
		this.getSolicitud();
	}

	getSolicitud(){
		this._route.params.forEach((params: Params) =>{
			let id = params['id'];

			this._solicitudService.getSolicitud(this.token, id).subscribe(
					response => {
						this.solicitud = response.solicitud;

						if (!response.solicitud) {
							this._router.navigate(['/']);
						}else{
							this.solicitud = response.solicitud;
						}
					},
					error =>{
						var errorMessage = <any>error;

			  			if (errorMessage != null) { 
			  				var body = JSON.parse(error._body);
			  				//this.alertMessage = body.message;
			  				
			  				console.log(error)
			  			}
					}
				);
		});
	}

	onSubmit(){
		console.log(this.solicitud);
		this._route.params.forEach((params: Params) =>{
			let id = params['id'];
			this._solicitudService.editSolicitud(this.token, id, this.solicitud).subscribe(
					response =>{
						this.solicitud = response.solicitud;

						if (!response.solicitud) {
							this.alertMessage = 'Error en el servidor';
						}else{
							this.alertMessage = 'La solicitud se ha actualizado correctamente';
							//this.solicitud = response.solicitud;
							//this._router.navigate(['/editar-solicitud'], response.solicitud._id);
							this._router.navigate(['/solicitudes',1]);
						}
					},
					error => {
						var errorMessage = <any>error;

			  			if (errorMessage != null) { 
			  				var body = JSON.parse(error._body);
			  				this.alertMessage = body.message;
			  				console.log(error)
			  			}
					}
				);
			}
		);
	}
}