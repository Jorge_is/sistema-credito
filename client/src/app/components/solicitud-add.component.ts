import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { SolicitudService } from '../services/solicitud.service';
import { Solicitud } from '../models/solicitud';

@Component({
	selector: 'solicitud-add',
	templateUrl: '../views/solicitud-add.html',
	providers: [UserService, SolicitudService]
})

export class SolicitudAddComponent implements OnInit {
	public titulo: string;
	public solicitud: Solicitud;
	public identity;
	public token;
	public url: string;
	public alertMessage;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _solicitudService: SolicitudService
	) {
		this.titulo = 'Crear nueva solicitud';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		this.solicitud = new Solicitud('','','','','','');
	}

	ngOnInit(){
		console.log('solicitud-add.component.ts cargado');
	}

	onSubmit(){
		console.log(this.solicitud);
		this._solicitudService.addSolicitud(this.token, this.solicitud).subscribe(
				response =>{
					this.solicitud = response.solicitud;

					if (!response.solicitud) {
						this.alertMessage = 'Error en el servidor';
					}else{
						this.alertMessage = 'La solicitud se ha creado correctamente';
						this.solicitud = response.solicitud;
						this._router.navigate(['/editar-solicitud', response.solicitud._id] );
					}
				},
				error => {
					var errorMessage = <any>error;

		  			if (errorMessage != null) { 
		  				var body = JSON.parse(error._body);
		  				this.alertMessage = body.message;
		  				console.log(error)
		  			}
				}
			);
	}
}