import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { Solicitud } from '../models/solicitud';

@Component({
	selector: 'home',
	templateUrl: '../views/home.html'
})

export class HomeComponent implements OnInit {
	public titulo: string;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router
	) {
		this.titulo = 'Solicitudes';
	}

	ngOnInit(){
		console.log('solicitud-list.component.ts cargado');

		//Conseguir el listado de solicitudes

	}
}