import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { SolicitudService } from '../services/solicitud.service';
import { Solicitud } from '../models/solicitud';

@Component({
	selector: 'solicitud-list',
	templateUrl: '../views/solicitud-list.html',
	providers: [UserService, SolicitudService]
})

export class SolicitudListComponent implements OnInit {
	public titulo: string;
	public solicitudes: Solicitud[];
	public identity;
	public token;
	public url: string;
	public next_page;
	public prev_page;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _solicitudService: SolicitudService
	) {
		this.titulo = 'Solicitudes';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		this.next_page = 1;
		this.prev_page = 1;
	}

	ngOnInit(){
		console.log('solicitud-list.component.ts cargado');

		//Conseguir el listado de solicitudes
		this.getSolicitudes();

	}

	getSolicitudes(){
		this._route.params.forEach((params: Params) => {
			let page = +params['page'];
			if (!page) {
				page = 1
			}else{
				this.next_page = page+1;
				this.prev_page = page-1;

				if (this.prev_page == 0) {
					this.prev_page = 1;
				}

				this._solicitudService.getSolicitudes(this.token, page).subscribe(
					response =>{
						if (!response.solicitudes) {
							this._router.navigate(['/']);
						}else{
							this.solicitudes = response.solicitudes;
						}
					},
					error => {
						var errorMessage = <any>error;

			  			if (errorMessage != null) { 
			  				var body = JSON.parse(error._body);
			  				//this.alertMessage = body.message;
			  				console.log(error)
			  			}
					}
				);
			}
		});
	}

	public confirmado;
	onDeleteConfirm(id){
		this.confirmado = id;
	}

	onCancelSolicitud(){
		this.confirmado = null;	
	}

	onDeleteSolicitud(id){
		this._solicitudService.deleteSolicitud(this.token, id).subscribe(
			response =>{
				if (!response.solicitud) {
						alert('Error en el servidor');
					}
					this.getSolicitudes();
			},
			error => {
						var errorMessage = <any>error;

			  			if (errorMessage != null) { 
			  				var body = JSON.parse(error._body);
			  				//this.alertMessage = body.message;
			  				console.log(error)
			  			}
			}
		);
	}
}