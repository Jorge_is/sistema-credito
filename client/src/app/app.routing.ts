import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//import user
import { HomeComponent } from './components/home.component';
import { UserEditComponent } from './components/user-edit.component';
//import solicitud
import { SolicitudListComponent } from './components/solicitud-list.component';
import { SolicitudAddComponent } from './components/solicitud-add.component';
import { SolicitudEditComponent } from './components/solicitud-edit.component';
import { SolicitudDetailComponent } from './components/solicitud-detail.component';

const appRoutes: Routes = [
	/*{
		path: '', 
		redirectTo: '/solicitudes/1',
		pathMatch: 'full'
	},*/
	{path: '', component: HomeComponent},
	{path: 'solicitudes/:page', component: SolicitudListComponent},
	{path: 'crear-solicitud', component: SolicitudAddComponent},
	{path: 'editar-solicitud/:id', component: SolicitudEditComponent},
	{path: 'solicitud/:id', component: SolicitudDetailComponent},
	{path: 'mis-datos', component: UserEditComponent},
	{path: '**', component: SolicitudListComponent}

];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);