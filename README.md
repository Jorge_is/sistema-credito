**README**
Requisitos para correr el proyecto:
- Tener instalado NODE JS
- Tener instalado MONGO DB
- Tener instalado Angular CLI
- Tener instalado NPM
- Tener instalado un Servidor Local


El Backend esta hecho en NODE.JS
EL Fronted esta hecho en Angular 2
La BD esta hecha en MONGO DB.

El proyecto esta divido en 2 capas, la parte de "api" que es el backend y el "client" que es el fronted.

Pasos para inicializar el proyecto:
1. Clonar el repositorio.
2. Entrar a la carpeta del proyecto y despues a la carpeta "api"
3. Estando en "api" correr el comando "npm install", para bajar las dependencias. 
4. Regresar a la carpeta raiz del proyecto y entrar a la carpeta "client"
5. Estando en "client", correr el comando "npm install"
6. Crear una carpeta en "C" si en windows, llamada "data", y dentro de "data" crear carpeta "db" (todo esto es para guardar los datos en la BD en Mongo DB). si es MAC  hacer lo mismo pero en disco duro,
7. Ejecutar el ejecutable "mongod.exe" y tambien el ejecutable "mongo.exe"
8. Entrar nuevamente a raiz, y entrar "api" y ejecutar el comando "npm start"
9. Entrar nuevamente a raiz, y entrar "client" y ejecutar el comando "npm start"

Si todos los pasos fueron exitosos, estara corriendo la aplicacion en "http://localhost:4200".