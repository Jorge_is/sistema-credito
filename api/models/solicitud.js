'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Creacion del Schema Solicitud
var SolicitudSchema = Schema({
	numero: String,
	fecha: String,
	personaFisica: String,
	monto: String,
	status: String,
	score: String
});

//Exportacion del modelo 'SolicitudSchema'
module.exports = mongoose.model('Solicitud', SolicitudSchema);