'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Creacion del Schema User
var UserSchema = Schema({
	name: String,
	surname: String,
	email: String,
	password: String,
	role: String
});

//Exportacion del modelo 'UserSchema'
module.exports = mongoose.model('User', UserSchema);