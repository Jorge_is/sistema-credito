'use strict'

var express = require('express');
var SolicitudController = require('../controllers/solicitud');
var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.get('/solicitud/:id', md_auth.ensureAuth, SolicitudController.getSolicitud);
api.post('/solicitud', md_auth.ensureAuth, SolicitudController.saveSolicitud);
api.get('/solicitudes/:page?', md_auth.ensureAuth, SolicitudController.getSolicitudes);
api.put('/solicitud/:id', md_auth.ensureAuth, SolicitudController.updateSolicitud);
api.delete('/solicitud/:id', md_auth.ensureAuth, SolicitudController.deleteSolicitud);


module.exports = api;