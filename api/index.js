'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3978;

mongoose.connect('mongodb://localhost:27017/sistema_credito', (err, res) =>{
	if (err) {
		throw err;
	} else {
		console.log("La conexion a la base de datos esta corriendo correctamente...");
		app.listen(port, function(){
			console.log("Servidor del api del sistema de credito escuchando en http://localhost:"+port);
		})
	}
});