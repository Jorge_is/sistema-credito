'use strict'

var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var jwt = require('../services/jwt');

//la 'req' es lo que va recibir en la peticion 
// y el 'res', es lo que va devolver este metodo
function pruebas(req, res) {
	res.status(200).send({
		message: 'Probando la ruta'
	});
}

function saveUser(req, res){
	var user = new User();

	var params = req.body;

	console.log(params);

	user.name = params.name;
	user.surname = params.surname;
	user.email = params.email;
	user.role = 'ROLE_USER';

	if (params.password) {
		//Encriptar contrasena y guardar datos
		bcrypt.hash(params.password, null, null, function(err, hash){
			user.password = hash;
			if (user.name != null && user.surname != null && user.email != null) {
				//Guardar el usuario en la BD
				user.save((err, userStored) =>{
					if (err) {
						res.status(500).send({message: 'Error al guardar el usuario'});
					} else {
						if (!userStored) {
							res.status(404).send({message: 'No se ha registrado el usuario'});
						} else {
							res.status(200).send({user: userStored});
						}
					}
				});
			} else {
				res.status(200).send({message: 'Rellena todos los campos'});
			}
		});
	} else {
		res.status(500).send({message: 'Introduce la contrasenia'});
	}

}

function loginUser(req, res) {
	var params = req.body;

	var email = params.email;
	var password = params.password;

	User.findOne({email	: email.toLowerCase()}, (err, user) =>{
		if (err) {
			res.status(500).send({message: 'Error en la peticion'});
		} else {
			if (!user) {
				res.status(404).send({message: 'El usuario no existe'});
			} else {
				//comprobar la contrasenia
				bcrypt.compare(password,user.password, function(err, check){
					if (check) {
						//devolver los datos del usuario logueado
						if (params.gethash) {
							//devolver un token de jwt
							res.status(200).send({
								token: jwt.createToken(user)
							});
						} else {
							res.status(200).send({user});
						}
					} else {
						res.status(404).send({message: 'El usuario no ha podido loguearse'});
					}
				} );
			}
		}
	});
}

function updateUser(req, res){
	var userId = req.params.id;
	var update = req.body;

	if (userId != req.user.sub) {
		return res.status(500).send({message: 'No tienes permisos'});
	}

	User.findByIdAndUpdate(userId, update, (err, userUpdated) =>{
		if (err) {
			res.status(500).send({message: 'Error al actualizar el usuario'});
		} else {
			if (!userUpdated) {
				res.status(404).send({message: 'No se ha podido actualizar el usuario'});
			} else {
				res.status(200).send({user: userUpdated});
			}
		}
	});
}

module.exports = {
	pruebas,
	saveUser,
	loginUser,
	updateUser
};