'use strict'

var path = require('path');
var fs = require('fs');
var mongoosePaginate = require('mongoose-pagination');

var Solicitud = require('../models/solicitud');

function getSolicitud(req, res) {
	var solicitudId = req.params.id;

	Solicitud.findById(solicitudId, (err, solicitud) =>{
		if (err) {
			res.status(500).send({message: 'Error en la peticion'});
		} else {
			if (!solicitud) {
				res.status(404).send({message: 'La solicitud no exite'});
			} else {
				res.status(200).send({solicitud})
			}
		}
	});	
}

function saveSolicitud(req, res) {
	//creo una nueva instancia de Solicitud
	var solicitud = new Solicitud();

	var params = req.body;
	solicitud.numero = params.numero;
	solicitud.fecha = params.fecha;
	solicitud.personaFisica = params.personaFisica;
	solicitud.monto = params.monto;
	solicitud.status = params.status;
	solicitud.score = params.score;

	solicitud.save((err, solicitudStored) => {
		if (err) {
			res.status(500).send({message: 'Error al guadar la solicitud'});
		} else {
			if (!solicitudStored) {
				res.status(404).send({message: 'La solicitud no ha sido guardada'});
			} else {
				res.status(200).send({solicitud: solicitudStored});
			}
		}
	});
}

function getSolicitudes(req, res) {
	if (req.params.page) {
		var page = req.params.page;
	} else {
		var page = 1;
	}
	
	var itemsPerPage = 6;

	Solicitud.find().sort('number').paginate(page, itemsPerPage, function(err, solicitudes, total) {
		if (err) {
			res.status(500).send({message: 'Error en la peticion'});
		} else {
			if (!solicitudes) {
				res.status(404).send({message: 'No hay artistas'});
			} else {
				return res.status(200).send({
					total_items: total,
					solicitudes: solicitudes
				});
			}
		}
	});
}

function updateSolicitud(req, res) {
	var solicitudId = req.params.id;
	var update = req.body;

	Solicitud.findByIdAndUpdate(solicitudId, update, (err, solicitudUpdated) =>{
		if (err) {
			res.status(500).send({message: 'Error al guardar la solicitud'});
		} else {
			if (!solicitudUpdated) {
				res.status(404).send({message: 'La solicitud no ha sido actualizada'});
			} else {
				res.status(200).send({solicitud: solicitudUpdated});
			}
		}
	});
}

function deleteSolicitud(req, res) {
	//guardo en una variable, el id que recibo por la URL
	var solicitudId = req.params.id;

	Solicitud.findByIdAndRemove(solicitudId, (err, solicitudRemoved) => {
		if (err) {
			res.status(500).send({message: 'Error en el servidor'});
		} else {
			if (!solicitudRemoved) {
				res.status(404).send({message: 'No ha se borrado la solicitud'});
			} else {
				res.status(200).send({solicitud: solicitudRemoved});
			}
		}
	});
}

module.exports = {
	getSolicitud,
	saveSolicitud,
	getSolicitudes,
	updateSolicitud,
	deleteSolicitud
}